#include "Bat.h"
#include "Settings.h"

Bat::Bat(float posx, float posy) : initPosx(posx), initPosy(posy) {
    frames[0] = sf::Texture();
    frames[1] = sf::Texture();
    frames[2] = sf::Texture();
    frames[0].loadFromFile("original/images/fruity_1.png");
    frames[1].loadFromFile("original/images/fruity_2.png");
    frames[2].loadFromFile("original/images/fruity_3.png");

    currentFrame = 0;

    sprite = sf::Sprite();
    sprite.setTexture(frames[0]);

    vy = 0;
    this->posy = posy;
    this->posx = posx;

    state = ALIVE;
}

void Bat::update(float dt) {
    // update the animation frame
    currentFrame += dt * FRAME_SPEED;
    while (currentFrame >= 3.0f) {
        currentFrame -= 3.0f;
    }

    // apply velocity
    posy += vy * dt;

    // apply acceleration
    vy += GRAVITY * dt;
}

void Bat::draw(sf::RenderTarget &target) {
    int frameIdx = int(floor(currentFrame)); // round doesn't work here, may round to 4
    sprite.setTexture(frames[frameIdx]);
    sprite.setPosition(posx - frames[frameIdx].getSize().x / 2,
                       posy - frames[frameIdx].getSize().y / 2);
    target.draw(sprite);

#ifndef NDEBUG
    // draw bounding box
    sf::RectangleShape bbox;
    bbox.setSize(sf::Vector2f(sprite.getTexture()->getSize()));
    bbox.setPosition(posx - frames[frameIdx].getSize().x / 2, posy - frames[frameIdx].getSize().y / 2);
    target.draw(bbox);
#endif
}

void Bat::jump() {
    if (state == DEAD) {
        return;
    }
    vy = - BAT_JUMP_SPEED;
}

void Bat::die() {
    state = DEAD;
    // initiate dieing animation
    vy = - BAT_JUMP_SPEED;
}

void Bat::reset() {
    state = ALIVE;
    posx = initPosx;
    posy = initPosy;
    vy = 0;
}

sf::FloatRect Bat::getRect() const {
    return sf::FloatRect(sprite.getPosition().x, sprite.getPosition().y,
                         sprite.getTexture()->getSize().x,
                         sprite.getTexture()->getSize().y - 20);
}
