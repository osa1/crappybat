#include "Utils.h"

#include <assert.h>
#include <stdlib.h>

int randRange(int begin, int end) {
    assert(end > begin);
    return begin + int(float(end - begin) * float(rand()) / float(RAND_MAX));
}
