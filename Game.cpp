#include <iostream>

#include "Game.h"
#include "Settings.h"
#include "Utils.h"

Game::Game() {
    std::cout << "Loading resources... ";
    backgroundImg = std::unique_ptr<Image>(new Image("original/images/background.png"));
    foregroundImg = std::unique_ptr<Image>(new Image("original/images/foreground.png"));
    obstacleTexture = std::unique_ptr<sf::Texture>(new sf::Texture());
    obstacleTexture->loadFromFile("original/images/obstacle.png");
    theBat = std::unique_ptr<Bat>(new Bat(backgroundImg->texture.getSize().x / 2,
                                          backgroundImg->texture.getSize().y / 2));
    obstacles = std::vector<Obstacle*>();
    std::cout << "Done." << std::endl;

    fgPos = 0;
    state = RUNNING;
}

Game::~Game() {
    // FIXME: Cleaning up the pointers manually until figuring how to iterate
    // vectors of unique ptrs that frees contents automatically.
    for (Obstacle *o : obstacles) {
        free(o);
    }
}

void Game::update(float dt) {
    if (state == RUNNING) {
        updateRunning(dt);
    } else { // state == RESTART
        updateDead(dt);
    }
}

void Game::updateRunning(float dt) {
    theBat->update(dt);
    updateObstacles(dt);
    updateBg(dt);

    // Check collisions
    {
        auto obstacle_iter = obstacles.begin();
        sf::FloatRect batRect = theBat->getRect();
        while (obstacle_iter != obstacles.end()) {
            if ((*obstacle_iter)->checkCollision(batRect)) {
                theBat->die(); // update bat state
                restart_in = 3.0f; // reset the game state in 3 seconds
                state = RESTART;
            }
            obstacle_iter++;
        }
    }

    // Add new obstacles
    while (obstacles.size() < 4) {
        Obstacle *o;
        if (obstacles.empty()) {
            // FIXME: Put magic numbers to Settings.h
            o = new Obstacle(*obstacleTexture,
                             400 /* adding some amount after the screen */,
                             240);
        } else {
            Obstacle *prev = obstacles[obstacles.size() - 1];
            float posx = prev->getPosx() + float(randRange(MIN_MARGIN, MAX_MARGIN));
            float center = prev->getCenter() + float(randRange(-MAX_CENTER_DIFF / 2, MAX_CENTER_DIFF / 2));

            // tweak the center, we don't want it to be too high or low
            // and make sure it's not out of the screen
            if (center < SPACE / 2 + 20) {
                center = SPACE / 2 + 20;
            }

            if (center > SCREEN_HEIGHT - SPACE / 2 - 20) {
                center = SCREEN_HEIGHT - SPACE / 2 - 20;
            }

            o = new Obstacle(*obstacleTexture, posx, center);
        }

        obstacles.push_back(o);
    }
}

void Game::updateBg(float dt) {
    fgPos -= (dt * FG_SPEED);
    if (fgPos < - float(backgroundImg->texture.getSize().x)) {
        fgPos += float(backgroundImg->texture.getSize().x);
    }
}

void Game::updateObstacles(float dt) {
    for (Obstacle *o : obstacles) {
        o->update(dt, BAT_SPEED_X);
    }

    // Remove obstacles
    {
        auto obstacle_iter = obstacles.begin();
        while (obstacle_iter != obstacles.end()) {
            if (!(*obstacle_iter)->checkInScreen()) {
                // TODO: Check if erase is calling destructor of erased object,
                // use a pointer that frees contents if that's the case.
                free(*obstacle_iter);
                obstacles.erase(obstacle_iter);
                std::cout << "erasing!" << std::endl;
            } else {
                obstacle_iter++;
            }
        }
    }
}

void Game::updateDead(float dt) {
    updateObstacles(dt);
    updateBg(dt);
    theBat->update(dt);

    restart_in -= dt;
    if (restart_in < 0) {
        theBat->reset();
        while (!obstacles.empty()) {
            // TODO: Is this really how I should reset and free contents of a vector?
            free(obstacles[obstacles.size() - 1]);
            obstacles.pop_back();
        }
        state = RUNNING;
    }
}

void draw_bg(sf::RenderTarget &target, Image &image) {
    target.draw(image.sprite);
}

void draw_fg(sf::RenderTarget &target, Image &image, float posx) {
    image.sprite.setPosition(posx, 0);
    target.draw(image.sprite);
    image.sprite.move(float(image.texture.getSize().x), 0);
    target.draw(image.sprite);
}

void Game::draw(sf::RenderTarget &target) const {
    draw_bg(target, *backgroundImg);
    draw_fg(target, *foregroundImg, fgPos);
    for (Obstacle *o : obstacles) {
        o->draw(target);
    }
    theBat->draw(target);
}

void Game::keyPressed(sf::Keyboard::Key code) {
    if (code == sf::Keyboard::Space) {
        theBat->jump();
    }
}
