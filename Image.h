#ifndef DEMO_IMAGE_H
#define DEMO_IMAGE_H

#include <SFML/Graphics.hpp>

// A combination of sf::Sprite and sf::Texture.
// (sf::Texture should be freed after sf::Sprite)
class Image {
public:
    // Sprite should be deleted first
    sf::Texture texture;
    sf::Sprite sprite;

    Image(const std::string &filename);
};

#endif //DEMO_IMAGE_H
