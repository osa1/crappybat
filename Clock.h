#ifndef DEMO_CLOCK_H
#define DEMO_CLOCK_H

#include <SFML/Graphics.hpp>

class Clock {
private:
    sf::Clock internalClock;
    sf::Time lastTick;

public:
    Clock();
    sf::Time tick();
};

#endif //DEMO_CLOCK_H
