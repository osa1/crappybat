#include <stdlib.h>

#include <SFML/Graphics.hpp>

#include "Clock.h"
#include "Game.h"
#include "Settings.h"

int main() {
    srand(time(0));

    Game game;
    Clock clock;
    sf::Event Event;

    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "demo",
                            sf::Style::Default & ~sf::Style::Resize);

    while (window.isOpen()) {
        while (window.pollEvent(Event)) {
            if (Event.type == sf::Event::Closed)
                window.close();
            else if (Event.type == sf::Event::KeyPressed) {
                game.keyPressed(Event.key.code);
            }
        }

        sf::Time dt = clock.tick();
        game.update(dt.asSeconds());

        window.setActive();
        window.clear(sf::Color::Black);
        game.draw(window);
        window.display();

        window.setTitle("FPS: " + std::to_string(std::floor(1 / dt.asSeconds())));
    }

    return 0;
}
