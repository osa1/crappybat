static const int SCREEN_WIDTH = 320;
static const int SCREEN_HEIGHT = 480;

// frame updates per second, specifies animation speed of bat
static const float FRAME_SPEED = 10.0f;

// px/sec^2, gravity that effects the bat
static const float GRAVITY = 1400.0f;

// px/sec, speed of bat
static const float BAT_SPEED_X = 180.0f;

static const float BAT_JUMP_SPEED = 350.0f;

// px/sec, speed of road in the background. smaller than BAT_SPEED_X
// to have a parallax-scrolling effect
static const float FG_SPEED = 50.0;

// minimum space between two obstacle columns
static const int MIN_MARGIN = 250;

// maximum space between two obstacle columns
static const int MAX_MARGIN = 400;

// maximum difference between centers of two consecutive columns
static const int MAX_CENTER_DIFF = 300;

// space between two obstacles in same column
static const int SPACE = 100;
