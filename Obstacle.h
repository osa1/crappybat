#ifndef DEMO_OBSTACLE_H
#define DEMO_OBSTACLE_H

#include <SFML/Graphics.hpp>

class Obstacle {
private:
    float width;
    float height;
    float posx;
    float center;

    sf::Sprite spriteTop, spriteBottom;
public:

    Obstacle(sf::Texture &texture, float posx, float center);
    // Poles don't move, so we take vx as argument
    void update(float dt, float vx);
    void draw(sf::RenderTarget &target) const;
    bool checkCollision(sf::Rect<float> &rect) const;
    bool checkInScreen() const;
    float getPosx() const;
    float getCenter() const;
};

#endif //DEMO_OBSTACLE_H
