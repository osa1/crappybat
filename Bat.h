#ifndef DEMO_BAT_H
#define DEMO_BAT_H

#include <SFML/Graphics.hpp>

class Bat {

enum BAT_STATE {
    DEAD,
    ALIVE
};

private:
    sf::Texture frames[3];
    sf::Sprite sprite;

    float currentFrame;
    float vy;
    const float initPosx;
    const float initPosy;
    float posy;
    float posx;

    BAT_STATE state;

public:
    Bat(float posx, float posy);
    void update(float dt);
    void draw(sf::RenderTarget &target);
    void jump();
    void die();
    void reset();
    sf::FloatRect getRect() const;
};

#endif //DEMO_BAT_H
