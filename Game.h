#ifndef DEMO_GAME_H
#define DEMO_GAME_H

#include <memory>
#include <vector>

#include <SFML/Graphics.hpp>

#include "Bat.h"
#include "Image.h"
#include "Obstacle.h"

enum GAME_STATE {
    RUNNING,
    RESTART
};

class Game {
private:
    std::unique_ptr<Bat> theBat;
    std::vector<Obstacle*> obstacles;

    std::unique_ptr<Image> backgroundImg;
    std::unique_ptr<Image> foregroundImg;
    std::unique_ptr<sf::Texture> obstacleTexture;

    float fgPos;

    GAME_STATE state;
    float restart_in;

    void updateRunning(float dt);
    void updateDead(float dt);
    void updateObstacles(float dt);
    void updateBg(float dt);

public:
    Game();
    ~Game();
    void update(float dt);
    void draw(sf::RenderTarget &target) const;
    void keyPressed(sf::Keyboard::Key code);
};

#endif //DEMO_GAME_H
