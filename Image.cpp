#include "Image.h"

Image::Image(const std::string &filename) {
    texture = sf::Texture();
    texture.loadFromFile(filename);
    sprite = sf::Sprite(texture);
}