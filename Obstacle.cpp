#include "Obstacle.h"
#include "Settings.h"

// center is the center of space between obstacles
Obstacle::Obstacle(sf::Texture &texture, float posx, float center) {
    this->posx = posx;
    this->center = center;
    this->width = texture.getSize().x;
    this->height = texture.getSize().y;

    spriteTop = sf::Sprite(texture);
    spriteTop.setOrigin(width / 2, height / 2);
    spriteTop.setPosition(posx, center - float(SPACE / 2) - height / 2);

    spriteBottom = sf::Sprite(texture);
    spriteBottom.setOrigin(width / 2, height / 2);
    spriteBottom.setRotation(180);
    spriteBottom.setPosition(posx, center + float(SPACE / 2) + height / 2);
}

void Obstacle::update(float dt, float vx) {
    posx -= vx * dt;
    spriteTop.setPosition(posx - width / 2, spriteTop.getPosition().y);
    spriteBottom.setPosition(posx - width / 2, spriteBottom.getPosition().y);
}

void Obstacle::draw(sf::RenderTarget &target) const {
    target.draw(spriteTop);
    target.draw(spriteBottom);

#ifndef NDEBUG
    sf::RectangleShape bboxTop;
    bboxTop.setSize(sf::Vector2f(spriteTop.getTexture()->getSize()));
    bboxTop.setOrigin(width / 2, height / 2);
    bboxTop.setPosition(spriteTop.getPosition());
    target.draw(bboxTop);

    sf::RectangleShape bboxBottom;
    bboxBottom.setSize(sf::Vector2f(spriteBottom.getTexture()->getSize()));
    bboxBottom.setOrigin(width / 2, height / 2);
    bboxBottom.setPosition(spriteBottom.getPosition());
    target.draw(bboxBottom);
#endif
}

bool Obstacle::checkCollision(sf::FloatRect &rect) const {
    return (spriteTop.getGlobalBounds().intersects(rect)
            || spriteBottom.getGlobalBounds().intersects(rect));
}

bool Obstacle::checkInScreen() const {
    return (spriteTop.getPosition().x >= - float(spriteTop.getTexture()->getSize().x / 2));
}

float Obstacle::getPosx() const { return posx; }

float Obstacle::getCenter() const { return center; }
