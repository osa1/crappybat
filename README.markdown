Yet another FlappyBird clone. This time writtten in C++11/SFML.

I'm a C++ and Gamedev n00b, so any feedback/comments/reviews would be appreciated.

Screenshot:

![screenshot](https://bytebucket.org/osa1/crappybat/raw/c4be50a0ec5f700587cf7e6fcde0155e79d624bd/ss.png)

Images are shamelessly stolen from https://github.com/tomdalling/fruity_bat

Building:

```
$ git clone https://osa1@bitbucket.org/osa1/crappybat.git
$ cd crappybat
$ mkdir build
$ cd build
$ cmake .. -DCMAKE_BUILD_TYPE=Release
$ make
$ cd ..
$ ./build/Demo
```

Note that if you forget to add `-DCMAKE_BUILD_TYPE=Release` it build in debug mode and shows
bounding boxes of objects instead of images.
