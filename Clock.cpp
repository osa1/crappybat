#include "Clock.h"

Clock::Clock() {
    internalClock = sf::Clock();
    lastTick = internalClock.getElapsedTime();
}

sf::Time Clock::tick() {
    sf::Time current = internalClock.getElapsedTime();
    sf::Time dt = current - lastTick;
    lastTick = current;
    return dt;
}